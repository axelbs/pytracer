import math

from hitable import Hitable
from vec3 import vec3

class Sphere(Hitable):
    
    #center = vec3
    #radius = float
    def __init__(self, cen, r):
        self.center = cen
        self.radius = r

    def hit(self, ray, t_min, t_max, rec):
        oc = ray.origin() - self.center
        a = float(vec3.dot(ray.direction(), ray.direction()))
        b = float(vec3.dot(oc, ray.direction()))
        c = float(vec3.dot(oc, oc)) - (self.radius * self.radius)
        discrimitant = (b * b) - (a * c)
        #print "osc:{} a:{} b:{} c:{} disc:{}".format(oc, a, b, c, discrimitant)

        if discrimitant > 0.0:
            temp = float((-b - math.sqrt(b*b - a*c)) / a)
            if temp < t_max and temp > t_min:
                rec.t = temp
                rec.p = ray.point_at_parameter(rec.t)
                rec.normal = (rec.p - self.center) / self.radius
                return True
            temp = float((-b + math.sqrt(b*b - a*c)) / a)
            if temp < t_max and temp > t_min:
                rec.t = temp
                rec.p = ray.point_at_parameter(rec.t)
                rec.normal = (rec.p - self.center) / self.radius
                return True
        return False
