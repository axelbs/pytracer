from vec3 import vec3

class Ray(object):

    #A = vec3(0.0, 0.0, 0.0)
    #B = vec3(0.0, 0.0, 0.0)

    def __init__(self, a, b):
        if not isinstance(a, vec3) and not isinstance(b, vec3):
            raise Exception
        self.A = a
        self.B = b

    def origin(self):
        return self.A

    def direction(self):
        return self.B

    def point_at_parameter(self, t):
        if not isinstance(t, float):
            raise Exception
        return self.A + (self.B * t)

    def __repr__(self):
        return "[*debug A] X:{} Y:{} Z:{} \n[*debug B] X:{} Y:{} Z:{}\n".format(
            self.A[0], self.A[1], self.A[2],
            self.B[0], self.B[1], self.B[2]
        )
