import math
from pypy import png 

from random import random

from camera import Camera
from hitable import hit_record
from hitable_list import HitableList
from sphere import Sphere
from ray import Ray
from vec3 import vec3

MAXFLOAT = 1.7976931348623157e+307

def write_png(nx, ny, p):
    f = open('out.png', 'wb')
    w = png.Writer(nx, ny)
    w.write(f, p)
    f.close()


def random_in_unit_sphere():
    p = (vec3(random(), random(), random()) * 2.0) - vec3(1.0, 1.0, 1.0)
    while vec3.squared_length(p) >= 1.0:
        p = (vec3(random(), random(), random()) * 2.0) - vec3(1.0, 1.0, 1.0)
    return p


def color(r, world):
    if not isinstance(r, Ray):
        raise Exception
    rec = hit_record(1.0, vec3(0, 0, 0), vec3(1.0, 1.0, 1.0))
    _test, rec = world.hit(r, 0.001, MAXFLOAT, rec)
    if _test:
        target = rec.p + rec.normal + random_in_unit_sphere()
        #_x, _y, _z = rec.normal.x, rec.normal.y, rec.normal.z
        return color(Ray(rec.p, target - rec.p), world) * 0.5
        #_ret_val = vec3(_x + 1, _y + 1, _z + 1) * 0.5
        #return _ret_val
    else:
        unit_direction = vec3.unit_vector(r.direction())
        t = (unit_direction.y + 1.0) * 0.5
        return (vec3(1.0, 1.0, 1.0) * (1.0 - t)) + (vec3(0.5, 0.7, 1.0) * t)


def pixels():
    nx = 200
    ny = 100
    ns = 100
    pixel_data = []

    lower_left_corner = vec3(-2.0, -1.0, -1.0)
    horizontal = vec3(4.0, 0.0, 0.0)
    vertical = vec3(0.0, 2.0, 0.0)
    origin = vec3(0.0, 0.0, 0.0)

    hit_list = [None, None]
    hit_list[0] = Sphere(vec3(0, 0, -1), 0.5)
    hit_list[1] = Sphere(vec3(0, -100.5, -1), 100.0)

    world = HitableList(hit_list, 2)
    cam = Camera()

    for j in xrange(ny, 0, -1):
        _line = []
        for i in xrange(0, nx, 1):
            col = vec3(0.0, 0.0, 0.0)
            for s in xrange(0, ns, 1):
                u = float(i + random()) / float(nx)
                v = float(j + random()) / float(ny)
                #print "u:{} * horizontal:{} = {}".format(u, horizontal, horizontal * u)
                #print "v:{} * vertical:{} = {}".format(v, horizontal, vertical * v)
                r = cam.get_ray(u, v)
                p = r.point_at_parameter(2.0)
                col += color(r, world)
                #print "[j:{} i:{}] r:{} g:{} b:{}".format(j,i,r,g,b)
            col = col / float(ns)

            # gamma-"correct" color squaring it
            col = vec3(math.sqrt(col[0]), math.sqrt(col[1]), math.sqrt(col[2]))
            ir = int(255.99 * col[0])
            ig = int(255.99 * col[1])
            ib = int(255.99 * col[2])
            #print "ir:{} * ig:{} ib:{}".format(ir, ig, ib)

            _line.append(ir)
            _line.append(ig)
            _line.append(ib)

        pixel_data.append(_line)
    write_png(nx, ny, pixel_data)


if __name__ == "__main__":
    import time
    delta = time.time()
    pixels()
    print "Time taken: {}".format((time.time() - delta))
