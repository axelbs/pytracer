import math

class vec3(object):

    def __init__(self, e0, e1, e2):
        self.e = [e0, e1, e2]
        self.x = self.e[0]
        self.y = self.e[1]
        self.z = self.e[2]
        self.r = self.e[0]
        self.g = self.e[1]
        self.b = self.e[2]

    def length(self):
        sqrt = math.sqrt
        return sqrt(e[0] * e[0] + e[1] * e[1] + e[2] * e[2])

    @staticmethod
    def length(v):
        sqrt = math.sqrt
        return sqrt(v.e[0] * v.e[0] + v.e[1] * v.e[1] + v.e[2] * v.e[2])

    @staticmethod
    def squared_length(v):
        return v.e[0] * v.e[0] + v.e[1] * v.e[1] + v.e[2] * v.e[2]
    
    @staticmethod
    def unit_vector(v):
        if not isinstance(v, vec3):
            raise Exception
        return v / vec3.length(v)

    @staticmethod
    def dot(v1, v2):
        return v1.e[0] * v2.e[0] + v1.e[1] * v2.e[1] + v1.e[2] * v2.e[2] 

    def __repr__(self):
        return "[*debug] X:{} Y:{} Z:{}".format(self.e[0], self.e[1], self.e[2])

    def __getitem__(self, index):
        return self.e[index]

    def __setitem__(self, index, value):
        self.e[index] = value

    def __add__(self, v2):
        if isinstance(v2, vec3):
            return vec3(
                self.e[0] + v2.e[0],
                self.e[1] + v2.e[1],
                self.e[2] + v2.e[2],
            )
        elif isinstance(v2, float):
            return vec3(
                self.e[0] + v2,
                self.e[1] + v2,
                self.e[2] + v2,
            )

    def __sub__(self, v2):
        if isinstance(v2, vec3):
            return vec3(
                self.e[0] - v2.e[0],
                self.e[1] - v2.e[1],
                self.e[2] - v2.e[2],
            )
        elif isinstance(v2, float):
            return vec3(
                self.e[0] - v2,
                self.e[1] - v2,
                self.e[2] - v2,
            )

    def __mul__(self, v2):
        if isinstance(v2, vec3):
            return vec3(
                self.e[0] * v2.e[0],
                self.e[1] * v2.e[1],
                self.e[2] * v2.e[2],
            )
        elif isinstance(v2, float):
            return vec3(
                self.e[0] * v2,
                self.e[1] * v2,
                self.e[2] * v2,
            )

    def __div__(self, v2):
        if isinstance(v2, vec3):
            return vec3(
                self.e[0] / v2.e[0],
                self.e[1] / v2.e[1],
                self.e[2] / v2.e[2],
            )
        elif isinstance(v2, float):
            return vec3(
                self.e[0] / v2,
                self.e[1] / v2,
                self.e[2] / v2,
            )

