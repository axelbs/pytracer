from vec3 import vec3
from hitable import Hitable
from hitable import hit_record

class HitableList(Hitable):
    
    def __init__(self, l, n):
        self._list = l
        self.list_size = n

    def hit(self, r, t_min, t_max, rec):
        temp_rec = hit_record(1.0,vec3(20.0,1.0,1.0),vec3(1.0,1.0,1.0))
        hit_anything = False
        closest_so_far = t_max

        for i in xrange(0, self.list_size):
            if self._list[i].hit(r, t_min, closest_so_far, temp_rec):
                hit_anything = True
                closest_so_far = temp_rec.t
                rec = temp_rec
        return hit_anything, rec
