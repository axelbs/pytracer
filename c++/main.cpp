#include <stdio.h>



int main(void) {
    int nx = 200;
    int ny = 100;

    for (int j = ny - 1; j >= 0; j--){
        for (int i = 0; i < nx; i++) {

            float r = float(i) / float(nx);
            float g = float(j) / float(ny);
            float b = 0.2f;

            printf("[j:%d i:%d] r:%f g:%f b:%f \n",j,i,r,g,b);
        
        }
    }
}
