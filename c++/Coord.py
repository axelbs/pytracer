import collections
from math import sqrt

# set up a Coord named tuple that we can use to store coordinates.
# Also adds support for adding and subtracting Coord instances.
Coord = collections.namedtuple("Coord", "x y z")


def coordAddSupport(self, other):
    if isinstance(other, Coord):
        return Coord(x=self.x + other.x, y=self.y + other.y)
    elif isinstance(other, int) or isinstance(other, float):
        return Coord(x=self.x + other, y=self.y + other)
    raise ValueError("Addition only by another Coord() or Int/Float")


def coordSubSupport(self, other):
    if isinstance(other, Coord):
        return Coord(x=self.x - other.x, y=self.y - other.y)
    elif isinstance(other, int) or isinstance(other, float):
        return Coord(x=self.x - other, y=self.y - other)
    raise ValueError("Subtract only by another Coord() or Int/Float")


def coordDivSupport(self, other):
    if isinstance(other, Coord):
        return Coord(x=self.x / other.x, y=self.y / other.y)
    elif isinstance(other, int) or isinstance(other, float):
        return Coord(x=self.x / other, y=self.y / other)
    raise ValueError("Divide only by another Coord() or Int/Float")


def coordMulSupport(self, other):
    if isinstance(other, Coord):
        return Coord(x=self.x * other.x, y=self.y * other.y)
    elif isinstance(other, int) or isinstance(other, float):
        return Coord(x=self.x * other, y=self.y * other)
    raise ValueError("Multiply only by another Coord() or Int/Float")


def coordToString(self):
    return "x={0},y={1}".format(self.x, self.y)


def coordEquals(self, other):
    if other is None:
        return False
    return self.x == other.x and self.y == other.y


def coordDistance(self, other):
    return sqrt((other.x - self.x) ** 2 + (other.y - self.y) ** 2)


def coordMagnitude(self):
    return sqrt(sum(self[i] * self[i] for i in range(len(self))))


def coordNormalize(self):
    vmag = self.magnitude()
    if vmag == 0:
        print("Division by zero... patching!")
        vmag = 1
    return Coord(x=self.x / vmag, y=self.y / vmag)


def coordNeighbours(self):
    return [self - Coord(x=1, y=0), self - Coord(x=-1, y=0),
            self - Coord(x=0, y=1), self - Coord(x=0, y=-1)]


def coordAsTuple(self):
    return (self.x, self.y)


Coord.__add__ = coordAddSupport
Coord.__sub__ = coordSubSupport
Coord.__div__ = coordDivSupport
Coord.__mul__ = coordMulSupport
Coord.__str__ = coordToString
Coord.__repr__ = coordToString
Coord.__eq__ = coordEquals
Coord.distance = coordDistance
Coord.neighbours = coordNeighbours
Coord.normalize = coordNormalize
Coord.magnitude = coordMagnitude
Coord.asTuple = coordAsTuple
