from ray import Ray
from vec3 import vec3

class Camera(object):

    def __init__(self):
        self.lower_left_corner = vec3(-2.0, -1.0, -1.0)
        self.horizontal = vec3(4.0, 0.0, 0.0)
        self.vertical = vec3(0.0, 2.0, 0.0)
        self.origin = vec3(0.0, 0.0, 0.0)
    
    #float u, float v
    def get_ray(self, u, v):
        a = self.origin
        b = self.lower_left_corner + self.horizontal*u + self.vertical*v - self.origin
        return Ray(a, b)
